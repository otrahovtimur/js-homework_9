// ТЕОРІЯ
/* 
1.DOM - це інтерфейс, модель якого побудована з структури об'єктів у формі дерева, і з цими об'єктами можна маніпулювати за допомогою різних методів.

2.
innerHTML - виводить теги разом з контентом, а також вводить нові теги та відтворює їх відповідно до їх функціоналу. 
innerText працює по принципу textContent. Він отримує лише контент без тегів та при введені нових тегів відображає їх як текст.

3.
getElement
querySelector
та їх різні версії , кращий спосіб залежить від поставленої задачі.


4.Це схожі на масив колекції вузлів. 
nodeList (querySelectorAll) може зберігати всі типи вузлів (тексти, коментарі).
HTMLColection (getElement...) може зберігати лише вузли HTML елементів.


*/

//   ПРАКТИКА

//1
const featureOne = document.getElementsByClassName('feature');
console.log(featureOne);
const featureTwo = document.querySelectorAll('.feature');
console.log(featureTwo);

for (let el of featureOne){
	el.style.textAlign = "center";
}

//2
const title = document.getElementsByTagName('h2');

for (let el of title){
	el.textContent = "Awesome feature";
}

//3
const featureTitle = document.getElementsByClassName('feature-title');

for ( let i=0; i<featureTitle.length; i++) {
    featureTitle[i].textContent += '!';
};
